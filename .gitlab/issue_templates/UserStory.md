## Summary

(Summarize the story in a quick overview)


## As a [BLANK], I want to [BLANK]

(What kind of user are you, what are you wanting or expecting to do - this is very important)


### Detailed Scope

(What actually happens)


## Example Project

(If possible, please create an example project here on GitLab.com that exhibits the problematic behaviour, and link to it here in the bug report)


### Relevant logs and/or screenshots, design files, links, etc

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)



/label ~user-story ~needs-investigation
/assign @aliendev
