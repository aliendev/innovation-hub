---
layout: product
name:  "Omate Lutetia"
tagline: "Technology is what you can carry; Fashion is what you wear"
description: "Omate's Lutetia smartwatch has been designed to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now."
image: "/assets/images/hp-grid/Omate.jpg"
productsInside: "XCORP7 processor"
partnersInside: "Ycomm"
date:   2015-08-01 15:32:14 -0300
categories: wearables
---
Smart glasses have the potential to enhance all sorts of industries by complementing the physical world with a new information-rich virtual platform. Atheer Labs believe their smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces.

Based in Mountain View, California, Atheer Labs has created a 3D augmented reality platform with gesture-based interactions for enterprise customers in the industrial, oil and gas and medical industries. Their platform consists of its AiR&trade; Smart Glasses and AiR OS, in addition to the AiR DK2 developer kit.

AiR provides users with accurate real-time information that will enable industry-specific advantages to companies. The device will be able to enable hands-free capabilities for field tech, assistance for customer service agents and access to patient data for on-the-go doctors.

Weighing less than 75 grams, AiR has a 65-degree field of view with 1048x785 resolution for each eye. The device uses dual 8-megapixel camera and a 3D depth sensor for sub-millimeter hand recognition accuracy, allowing users to physically control what is in front of them. The device connects to smartphones via micro-USB and can output 3D video and respond to gesture, voice and head motion commands.

The device contains the XCORP&reg;-based Ycomm&reg; SnappingTurtle&reg; 8000 processor. The processor was chosen as it provides energy-efficient advanced video, camera and audio experiences for users.
