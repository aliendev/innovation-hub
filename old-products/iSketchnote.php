<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - iSketchnote</title>

    <meta name="description" content="The iSketchnote slate integrates new digitizing technology with the convenience of a paper pad. With iSketchnote, you've got the pleasure of your drawings or notes on paper with a digital copy to share electronically.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, isketchnote, smart doodle, smart sketching, tablet sketching, tablet doodling, smart drawing, tablet drawing, virtual drawing, drawing technology, sketching technology, virtual sketching">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - iSketchnote" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/iSketchnote.php" />
    <meta property="og:description" content="The iSketchnote slate integrates new digitizing technology with the convenience of a paper pad. With iSketchnote, you've got the pleasure of your drawings or notes on paper with a digital copy to share electronically." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/iskn/iskn-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - iSketchnote" />
    <meta name="twitter:description" content="The iSketchnote slate integrates new digitizing technology with the convenience of a paper pad. With iSketchnote, you've got the pleasure of your drawings or notes on paper with a digital copy to share electronically." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/iskn/iskn-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/iSketchnote.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - iSketchnote">
    <meta itemprop="description" content="The iSketchnote slate integrates new digitizing technology with the convenience of a paper pad. With iSketchnote, you've got the pleasure of your drawings or notes on paper with a digital copy to share electronically.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/iskn/iskn-sm.jpg">
    

    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>


<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">iSketchnote</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="/innovation/assets/images/products/iskn/iskn4.jpg" alt="iskn"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/iskn/iskn1.jpg" alt="iskn"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/iskn/iskn2.jpg" alt="iskn"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/iskn/iskn3.jpg" alt="iskn"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>iSketchnote</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Digitize your doodles!</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>Doodling and drafting ideas with pen and paper gets the creative juices flowing. Somehow with all their technological sensors, tablets just aren't conducive to written ramblings and sketches as a blank sheet and a ballpoint. But, wouldn't it be great if there was a crossover that would let us mimic our old-fashioned ways in the digital world? </p>
      <p>The Slate by ISKN is the answer. It gives you the notion of pen and paper, evoking the indulgence of scribbles, re-writes and crafting sentences without pressing a delete button in frustration.  With iSketchnote, you've got the secretive pleasure of your records on paper and a digital copy to share with friends on social media! </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/4XrxK-Sdrwg?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>An array of sensors within iSketchnote capture your pen strokes on a piece of paper or notepad placed over the front of your tablet. You can use any piece paper with an ISKN ballpoint pen as the permanent ring-shaped magnet inside will record your musings.  The iSketchnote can even detect the color and tip of the pen you are using so you aren't tied to old-school black or blue!   </p>
      <p>The digitizer uses a STM32 family MCU from STMicroelectronics based on the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M4 processor. A 4GB SD card will hold around 100 pages, so you will always have plenty of space to scribble ideas and drawings for safe keeping. </p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.isketchnote.com/" target="_blank">iskn</a></p>
        <img src="/innovation/assets/images/products/iskn/iskn-logo.jpg" width="182" height="100" alt="iskn logo" class="logos image"/> </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m4-processor.php" target="_blank">ARM Cortex-M4</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>