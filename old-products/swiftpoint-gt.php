<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Swiftpoint GT</title>

    <meta name="description" content="The Swiftpoint GT has been shaped to be used like a pen rather than a traditional mouse with a claw-grip and, unlike traditional 'point and click' mice, it enables users to steer the mouse in a way similar to that of a finger touching a screen.">
    <meta name="keywords" content="arm, innovation, innovation hub, Swiftpoint GT, smart mouse, Bluetooth mouse,  travel mouse, Bluetooth mouse, touch gesture mouse, precision mouse">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Swiftpoint GT" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/swiftpoint-gt.php" />
    <meta property="og:description" content="The Swiftpoint GT has been shaped to be used like a pen rather than a traditional mouse with a claw-grip and, unlike traditional 'point and click' mice, it enables users to steer the mouse in a way similar to that of a finger touching a screen." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/swiftpoint/swiftpoint-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Swiftpoint GT" />
    <meta name="twitter:description" content="The Swiftpoint GT has been shaped to be used like a pen rather than a traditional mouse with a claw-grip and, unlike traditional 'point and click' mice, it enables users to steer the mouse in a way similar to that of a finger touching a screen." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/swiftpoint/swiftpoint-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/swiftpoint-gt.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Swiftpoint GT">
    <meta itemprop="description" content="The Swiftpoint GT has been shaped to be used like a pen rather than a traditional mouse with a claw-grip and, unlike traditional 'point and click' mice, it enables users to steer the mouse in a way similar to that of a finger touching a screen.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/swiftpoint/swiftpoint-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Swiftpoint GT</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/swiftpoint/swiftpoint1.jpg" alt="Swiftpoint GT"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/swiftpoint/swiftpoint2.jpg" alt="Swiftpoint GT"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/swiftpoint/swiftpoint3.jpg" alt="Swiftpoint GT"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/swiftpoint/swiftpoint4.jpg" alt="Swiftpoint GT"/>
        <div class="container"> </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Swiftpoint GT</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Redefining the mouse for mobile users</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">April 1, 2015</p>
      <p>If  you've ever tried using a mouse on aircraft or a packed train you'll know why  most travel computing is done using a touch screen or touch pad. Yet sometimes  you really do need a mouse for intricate gestures or edits and that's the  challenge a new portable pen/mouse hybrid device is trying to solve.</p>
      <p>According  to its developers, the Swiftpoint GT is the World's first mouse allowing  gesturing with natural finger and wrist actions without having to touch a  screen. The GT has been shaped to be used like a pen rather than a traditional  mouse with a claw-grip and, unlike traditional 'point and click' mice, the GT has  a TouchGesture<sup>&trade;</sup> mode enabling users to steer the mouse in a way similar to that  of a finger touching a screen. Swiftpoint say navigation around large  spreadsheets and documents becomes simple while application switching becomes  fast, intuitive and easy. </p>
      <p>While  touchscreen devices are ideal for watching media content, they can become problematic  when creating and editing content. The GT developers claim their device is  30-40 percent more effective* and accurate than touchpads, enabling  editors to increase their productivity and work faster.  <em><span style="font-size:.6em">*based on  industry-standard Fitts' Law test</span></em></p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/MY6weQACgm4?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Featuring  an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M0 processor on an nRF51822 SoC from Nordic  Semiconductor to provide energy-efficiency and low-power usage, the wireless  mouse connects to your computer via Bluetooth<sup>&reg;</sup> or USB receiver which  is also used to charge the mouse. The device features RapidCharge<sup>&trade;</sup>  technology, providing up to an hour's worth of usage after 30 seconds of charge  time. The device is small and lightweight, measuring 2.2&quot; x 1.7&quot; x 1.3&quot; and  weighing only 23 grams.</p>
      <p>The  Swiftpoint GT won the best 'Computer Peripheral' product category at the 2015  CES Innovation Awards. The judges based their votes on overall engineering  qualities related to technical specifications and materials, aesthetics and  design qualities; the product's intended use and function; its unique features  and how the design and innovation of the product compares to others in the  marketplace.</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://eu.swiftpoint.com/" target="_blank">Swiftpoint</a></p>
        <img src="/innovation/assets/images/products/swiftpoint/swiftpoint-logo.jpg" alt="Swiftpoint logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-nordic-semiconductor-asa" target="_blank">Nordic Semiconductor</a></p>
        <img src="/innovation/assets/images/partner-logos/nordic-semiconductor-logo.jpg" class="logos image" />         
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m0.php" target="_blank">ARM Cortex-M0</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>