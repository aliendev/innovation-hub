<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Haiku</title>

    <meta name="description" content="Haiku® with SenseME™ is the world's first smart ceiling fan. Designed with an onboard computer and array of sensors, this fan makes decisions to keep you comfortable – automatically. SenseME monitors environmental conditions, senses occupancy and learns user preferences to make adjustments. ">
    <meta name="keywords" content="arm, innovation, innovation hub, Big Ass Fans, Haiku, SenseME, Haiku SenseME, smart home, fans, fan, automated fan, intelligent ceiling fan, fan technology, smart ceiling fans, intelligent home">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Haiku" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/haiku.php" />
    <meta property="og:description" content="Haiku® with SenseME™ is the world’s first smart ceiling fan. Designed with an onboard computer and array of sensors, this fan makes decisions to keep you comfortable – automatically." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/haiku/haiku-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Haiku" />
    <meta name="twitter:description" content="Haiku® with SenseME™ is the world’s first smart ceiling fan. Designed with an onboard computer and array of sensors, this fan makes decisions to keep you comfortable – automatically." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/haiku/haiku-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/haiku.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Haiku">
    <meta itemprop="description" content="Haiku® with SenseME™ is the world’s first smart ceiling fan. Designed with an onboard computer and array of sensors, this fan makes decisions to keep you comfortable – automatically.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/haiku/haiku-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Haiku</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/haiku/haiku1.jpg" alt="Haiku"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/haiku/haiku2.jpg" alt="Haiku"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/haiku/haiku3.jpg" alt="Haiku"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/haiku/haiku4.jpg" alt="Haiku"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/haiku/haiku5.jpg" alt="Haiku"/>
        <div class="container"> </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Haiku</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>The ceiling fan that aims to save money</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">April 1, 2015</p>
      <p>A ceiling fan that keeps you  warm in the winter seems an oddity but it's actually a great use of air flow  physics. The Haiku<sup>&reg;</sup> with SenseME<sup>&trade;</sup> from Big Ass Fans can  save households up to 25 percent on their heating bills in the colder months by  automating the fan to gently push warm air down to occupant level via its  'Winter Mode'.</p>
      <p>The fan is controlled by a  smartphone app that contains a number of built-in modes to cater for user's  comforts. It can start up or slow down as you enter or leave a room, speed up  as more people enter a room or slow down when the temperature drops in the  evening. Over time, it learns your behaviors and will save you money automatically.</p>
      <p>Available in either aluminum,  Moso bamboo or matrix composite airfoils, Haiku uses a built-in Wi-Fi module,  infrared motion, ambient temperature and humidity sensors to control and  monitor the temperature of a room to the user's ideal temperature.</p>
      <p>Unlike traditional ceiling  fans, the Haiku has various modes that make your life more comfortable. 'Sleep  Mode' is customizable to each user's preference and can slow the speed of the  fan as temperatures drop overnight. 'Whoosh Mode' cycles through Haiku's seven  speeds to mimic natural breezes, a feature that Big Ass Fans states will make  you feel 40 percent cooler. Finally, there is an 'Alarm' mode that can be  programed to wake users up with a combination of air, light and sound using the  39 integrated LED lights with 16 levels of brightness.</p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/p0QvlQRm4W0?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The Haiku features the MK10DN128VFT5 SoC from  Freescale's Kinetis K10 sub-family. The K10 sub family is based on an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M4 processor and was chosen by Big Ass Fans due to its  energy-efficiency and low-power consumption. This efficiency is shown through  Haiku holding the top ENERGY STAR<sup>&reg;</sup> rating for ceiling fans.</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.bigassfans.com/" target="_blank">Big Ass Fans</a></p>
        <img src="/innovation/assets/images/products/haiku/big-ass-fans-logo.jpg" alt="Big Ass Fans logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-freescale" target="_blank">Freescale</a></p>
        <img src="/innovation/assets/images/partner-logos/freescale-logo.jpg" class="logos image" />         
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m4-processor.php" target="_blank">ARM Cortex-M4</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>