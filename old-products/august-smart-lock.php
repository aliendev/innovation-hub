<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - August Smart Lock</title>

    <meta name="description" content="The Smart Lock from August prevents you from being locked out of your home by being a virtual doorman. The device allows you to set keys and provide access to your home to anyone in your address book via your smartphone or tablet.">
    <meta name="keywords" content="arm, innovation, innovation hub, august smart lock, smart lock, virtual doorman, door lock technology, smart security system, automated lock, virtual key, Bluetooth home, intelligent home">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - August Smart Lock" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/august-smart-lock.php" />
    <meta property="og:description" content="The Smart Lock from August cannot prevent people from losing their keys but it does stop people from being locked out by being a virtual doorman." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/august-smart-lock/august-smart-lock-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - August Smart Lock" />
    <meta name="twitter:description" content="The Smart Lock from August cannot prevent people from losing their keys but it does stop people from being locked out by being a virtual doorman." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/august-smart-lock/august-smart-lock-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/august-smart-lock.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - August Smart Lock">
    <meta itemprop="description" content="The Smart Lock from August cannot prevent people from losing their keys but it does stop people from being locked out by being a virtual doorman.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/august-smart-lock/august-smart-lock-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">August Smart Lock</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/august-smart-lock/august-lock4.jpg" alt="August Smart Lock"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/august-smart-lock/august-lock2.jpg" alt="August Smart Lock"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/august-smart-lock/august-lock3.jpg" alt="August Smart Lock"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/august-smart-lock/august-lock1.jpg" alt="August Smart Lock"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>August Smart Lock</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Your  mobile is key to your door!</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>You've remembered your  phone, turned off the TV and the front door has just shut behind you. The  problem is you've just realized you've forgotten your keys and your family with  the spare key are on holiday. Your only option is paying a locksmith a fortune  to unlock your house. This sort of memory lapse is all too common, with one in  four of us locking ourselves out of our homes every year. Wouldn't it be great  if the phone you did remember could also get you back in? Well, help is at hand…</p>
      <p>The August Smart Lock delivers a simple,  seamless, and secure home entry experience unlike any other currently on the market.  The lock retrofits to your existing deadbolt and is powered by an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M3  processor on a STM32 family MCU from STMicroelectronics. It requires no wiring  and enables users to manage home entry through a Bluetooth® enabled locking  mechanism and an intuitive mobile application.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/cf9DRZLEVTM?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>As you can imagine,  security is high on the agenda for August. The device relies on algorithmically-generated  keys similar to a secure ID token, so the device does not need an Internet  connection at all. August sends all necessary online communications though the  paired phone, which uses the same communication encryption protocols that are  commonly used for online banking.</p>
      <p>Homeowners have the  ability to invite and issue virtual keys. Friends and family can be set to have  full access while temporary visitors such as plumbers and electricians can be  provided with access over a set period of time. Each entry is recorded so  owners can review who has been in their house.</p>
      <p>And don't worry if your  phone battery dies – you are still able to access your house with an  old-fashioned key or you can borrow a friend's phone and download the app to  open the door.</p>
      <p>Hi-tech security is no longer the domain of the  rich as August is the 21st century way of accessing your home. Keys  will soon be a thing of the past and keyrings will become the domain of  collectors!</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.august.com/" target="_blank">August</a></p>
        <img src="/innovation/assets/images/products/august-smart-lock/august-logo.jpg" alt="August logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php" target="_blank">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>