<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Omate Lutetia</title>

    <meta name="description" content="Omate’s Lutetia smartwatch has been designed to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now.">
    <meta name="keywords" content="arm, innovation, innovation hub, wearables, female smartwatch, jewelry smartwatch, fashion wearables">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Omate Lutetia" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/omate.php" />
    <meta property="og:description" content="Omate’s Lutetia smartwatch has been designed to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/omate/omate-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Omate Lutetia" />
    <meta name="twitter:description" content="Omate’s Lutetia smartwatch has been designed to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/omate/omate-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/omate.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Omate Lutetia">
    <meta itemprop="description" content="Omate’s Lutetia smartwatch has been designed to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/omate/omate-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Omate Lutetia</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/omate/omate2.jpg" alt="Omate Lutetia"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/omate/omate3.jpg" alt="Omate Lutetia"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/omate/omate1.jpg" alt="Omate Lutetia"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Omate Lutetia</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>The smartwatch with true feminine style</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">June 1, 2015</p>
      <p>While most Smartwatches are designed for all genders, many remain more appealing to men because of their size, weight and shape. Omate has tackled this with a new slimline design called the Lutetia which is specifically designed to be more appealing to women.</p> 
<p>Omate, A smartwatch manufacturer from Mountain View, California, has created the Lutetia Smartwatch, a new take on the design choices currently being offered. The intention is to be more appealing to women who want a smartwatch but do not like the styling of the models widely available now.</p>
<p>The Lutetia has the design and feel of jewelry rather than a piece of technology as it is crafted out of stainless steel and comes in silver, gold and rose gold variants. The watch face measures 40mm in diameter and is slim at just under an inch in thickness thick with only one button on the right side to maintain its elegance and fashionable design.</p> 
<p>The Lutetia pairs with smartphones via a Bluetooth<sup>&reg;</sup> Low Energy 4.0 connection to push notifications for emails, incoming calls and social media updates to a user’s wrist and has an always-on capacitive touch-screen that changes to various light conditions. The all-in-one watch also features an alarm, stopwatch, timer functions and pedometer capabilities to assist with day-to-day lives.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/e_UKONgKbEQ?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Omate's smartwatches are powered by the MediaTek Aster (MT2502) SoC that features the ARM7<sup>&trade;</sup> processor. The Aster chip was chosen by Omate as it is the world's smallest commercial SoC for wearable technology.</p>  
<p>To maintain the Lutetia’s exclusivity, the first batch of devices will receive limited edition status, with each wearable being engraved with a number from 1-1000.
</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.omate.com/" target="_blank">Omate</a></p>
        <img src="/innovation/assets/images/products/omate/omate-logo.jpg" alt="Omate logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-mediatek" target="_blank">MediaTek</a></p>
        <img src="/innovation/assets/images/partner-logos/mediatek-logo.jpg" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/classic/arm7/index.php" target="_blank">ARM7</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>