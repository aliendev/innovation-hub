<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - Rufus Cuff</title>

    <meta name="description" content="Rufus Cuff delivers Web, messaging, apps, voice and video calls on your wrist. With a 3-inch screen made of Corning Gorilla glass, an unconventional design and a new form factor, the Rufus Cuff is being heralded as a 'Wrist Communicator and might just take wearable technology into a new direction.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, rufus cuff, smart watch, wrist communicator, wearable technology, wearables">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Rufus Cuff" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/rufus-cuff.php" />
    <meta property="og:description" content="Rufus Cuff delivers Web, messaging, apps, voice and video calls on your wrist. With a 3-inch screen made of Corning Gorilla glass, an unconventional design and a new form factor, the Rufus Cuff is being heralded as a 'Wrist Communicator' and might just take wearable technology into a new direction." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/rufus-cuff/rufus-cuff-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Rufus Cuff" />
    <meta name="twitter:description" content="Rufus Cuff delivers Web, messaging, apps, voice and video calls on your wrist. With a 3-inch screen made of Corning Gorilla glass, an unconventional design and a new form factor, the Rufus Cuff is being heralded as a 'Wrist Communicator' and might just take wearable technology into a new direction." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/rufus-cuff/rufus-cuff-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/rufus-cuff.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Rufus Cuff">
    <meta itemprop="description" content="Rufus Cuff delivers Web, messaging, apps, voice and video calls on your wrist. With a 3-inch screen made of Corning Gorilla glass, an unconventional design and a new form factor, the Rufus Cuff is being heralded as a 'Wrist Communicator' and might just take wearable technology into a new direction.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/rufus-cuff/rufus-cuff-sm.jpg">
    

    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>



<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Rufus Cuff</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    


<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/rufus-cuff/Rufus-Cuff4.jpg" alt="Rufus Cuff"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/rufus-cuff/Rufus-Cuff2.jpg" alt="Rufus Cuff"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/rufus-cuff/Rufus-Cuff3.jpg" alt="Rufus Cuff"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/rufus-cuff/Rufus-Cuff1.jpg" alt="Rufus Cuff"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Rufus Cuff</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>It's all in the wrist</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>If you're looking for wearable technology that Q from James Bond movies would be proud of then the Rufus Cuff is it!  Forget a straightforward smartwatch, the new Rufus Cuff delivers Web, messaging, apps, voice and video calls on your wrist. With a 3-inch screen made of Corning Gorilla glass, an unconventional design and a new form factor, the Rufus Cuff is being heralded as a &quot;Wrist Communicator&quot; and might just take wearable technology into a new direction. </p>
      <p>Fired-up by an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-A9 core on an AM4378 processor from TI, the Rufus Cuff has been designed to relegate your smartphone to your pocket as a &quot;hub&quot; synced to the device. Thanks to the vibration call alert, with just one touch you can answer and dial voice or video calls direct from your wrist. Messages can be checked in seconds and replied to using the full-screen keyboard or dictated via your smartphone's voice support system.  </p>
      <p>With GPS, an accelerometer and a gyroscope, the Rufus Cuff allows users to download ﬁtness apps such as RunTracker and Endomondo to record steps taken, calories burned and distance traveled. If you're taking a break from your exercise regime and want to catch your breath, simply connect the Rufus Cuff to Wi-Fi<sup>&reg;</sup> to check your emails, messages, social media, calendars or make VoIP and video calls even though your smartphone isn't connected!  </p>
      <p>Listening to your favorite music songs is easy as the Bluetooth<sup>&reg;</sup> 4.0 enabled Rufus Cuff works with wireless headphones so you can access playlists stored on the device or stream songs from your smartphone. So whether you're a fan of Beyoncé or Metallica you can sing-along or practice your air guitar with the hands-free Rufus Cuff! </p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/q5Q9ua0fnY0?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>So what else is included in the device? The <a href="http://vimeo.com/rufuslabs" target="_blank">Rufus Cuff</a> contains a 3-inch TFT capacitive touch-screen (400 x 240 QVGA resolution), a camera, LED alerts and a flashlight. The device is spill and splash resistant with a choice of 16, 32 &amp; 64GB storage space and multi-language support as standard. The 900mAH rechargeable battery provides a day's worth of battery life.  </p>
      <p>Where could we go with this technology in the future? Imagine a world where searching in a bag or pocket to answer your phone was a thing of the past. Playing motion control games will allow gamers to experience a new level of immersive gaming with the freedom of hands-free. Removing everything from our pockets and reinvigorating the state of human interaction is the ultimate goal at Rufus Labs &ndash; so watch this space! </p>
      <p><a href="http://community.arm.com/groups/wearables/blog/2015/02/20/the-rufus-cuff--reimagining-the-world-of-wearable-technology" target="_blank">Read Rufus Labs' guest blog on the ARM Connected Community.</a></p>
</div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://rufuslabs.com/" target="_blank">Rufus Cuff</a></p>
        <img src="/innovation/assets/images/products/rufus-cuff/rufus-cuff-logo.jpg" alt="Rufus Cuff logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-texas-instruments" target="_blank">Texas Instruments</a></p>
        <img src="/innovation/assets/images/partner-logos/texas-instruments-logo.jpg" alt="Texas Instruments logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-a/cortex-a9.php" target="_blank">ARM Cortex-A9</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>