<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Lantern</title>

    <meta name="description" content="The Lantern is a free content-distribution system to provide vast libraries of knowledge that can help societies with agriculture, education and health. Its aim is to make the World Wide Web truly global for the first time.">
    <meta name="keywords" content="ARM, innovation, innovation hub, Outernet, lantern, pocket library, smart device, wifi library, connected world, technology innovation, education, technology Africa, Wi-Fi library, solar library, solar technology, ARM solar">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Lantern" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/lantern.php" />
    <meta property="og:description" content="The Lantern is a free content-distribution system to provide vast libraries of knowledge that can help societies with agriculture, education and health. Its aim is to make the World Wide Web truly global for the first time." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/lantern/lantern-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Lantern" />
    <meta name="twitter:description" content="The Lantern is a free content-distribution system to provide vast libraries of knowledge that can help societies with agriculture, education and health. Its aim is to make the World Wide Web truly global for the first time." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/lantern/lantern-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/lantern.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Lantern">
    <meta itemprop="description" content="The Lantern is a free content-distribution system to provide vast libraries of knowledge that can help societies with agriculture, education and health. Its aim is to make the World Wide Web truly global for the first time.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/lantern/lantern-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Lantern</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/lantern/lantern2.jpg" alt="Lantern"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lantern/lantern3.jpg" alt="Lantern"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lantern/lantern4.jpg" alt="Lantern"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lantern/lantern5.jpg" alt="Lantern"/>
        <div class="container"> </div>
      </div>      
      <div class="item"><img src="/innovation/assets/images/products/lantern/lantern1.jpg" alt="Lantern"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Lantern</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Connecting the World to the Internet</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">May 1, 2015</p>
      <p>The World Wide Web is at the moment incorrectly named. Only a third of the world actually has an Internet connection, with 4.3 billion people left outside the connectivity circle. This is a fundamental problem that the global broadcast data company Outernet is tackling through its new offering, the Lantern.</p>
<p>The Lantern has been designed as a free content-distribution system to provide vast libraries of knowledge that can help societies with agriculture, education and health. Its aim is to make the World Wide Web truly global for the first time.</p>
<p>The system works similar to a modern radio, based on satellite communication. Outernet sends information to low-orbit satellites which bounces the data back down to receivers. The Lantern receivers pick up the radio waves and transfer them into digital files, such as Web pages, videos, audio and e-books. The Lantern device then turns the information into Wi-Fi links and works as a Wi-Fi hot spot to transfer the information to it can be viewed on smartphones, tablets or computers.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/ammanyLM_ko?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The Lantern features solar panels on each side to obtain optimum exposure to sunlight and has a maximum battery life of 12 hours when passively receiving data and 4 hours when its Wi-Fi signal is activated. The device contains a micro USB input so smartphones can be charged and a micro SD card slot so downloaded libraries can be saved. It is roughly the size of a flashlight and has a plastic case, this sturdy and robust design enables the device to be used both outside and inside.</p> 
<p>The Lantern is powered by a TI Sitara<sup>&trade;</sup>AM335 processor that utilizes the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-A8 core. Outernet chose ARM technology due to the energy-efficiency and low-power consumption benefits that allows the battery life of the device to be maximized between charges. 
</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://www.outernet.is/en/" target="_blank">Outernet</a></p>
        <img src="/innovation/assets/images/products/lantern/outernet-logo.jpg" alt="Outernet logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-texas-instruments" target="_blank">Texas Instruments</a></p>
        <img src="/innovation/assets/images/partner-logos/texas-instruments-logo.jpg" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-a/cortex-a8.php" target="_blank">ARM Cortex-A8</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>