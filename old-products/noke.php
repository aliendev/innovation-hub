<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Noke</title>

    <meta name="description" content="The Noke (pron. 'No Key') padlock is the World's first Bluetooth Padlock. By using Noke and your smartphone, you'll always have easy access to all the important things in your life. In addition, you'll be able to share access to your possessions easily, safely, and securely.">
    <meta name="keywords" content="arm, innovation, innovation hub, FUZ Designs, Noke, smart lock, smart device, Bluetooth gadget, Bluetooth bike, Smart Home, U-Lock, Smart bike lock,  Bluetooth lock, sharing lock, smart">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Noke" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/noke.php" />
    <meta property="og:description" content="The Noke (pron. 'No Key') padlock is the World's first Bluetooth Padlock. By using Noke and your smartphone, you'll always have easy access to all the important things in your life. In addition, you'll be able to share access to your possessions easily, safely, and securely." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/noke/noke-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Noke" />
    <meta name="twitter:description" content="The Noke (pron. 'No Key') padlock is the World's first Bluetooth Padlock. By using Noke and your smartphone, you'll always have easy access to all the important things in your life. In addition, you'll be able to share access to your possessions easily, safely, and securely." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/noke/noke-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/noke.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Noke">
    <meta itemprop="description" content="The Noke (pron. 'No Key') padlock is the World's first Bluetooth Padlock. By using Noke and your smartphone, you'll always have easy access to all the important things in your life. In addition, you'll be able to share access to your possessions easily, safely, and securely.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/noke/noke-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Noke</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/noke/Noke1.jpg" alt="Noke"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/noke/Noke2.jpg" alt="Noke"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/noke/Noke3.jpg" alt="Noke"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/noke/Noke4.jpg" alt="Noke"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/noke/Noke5.jpg" alt="Noke"/>
        <div class="container"> </div>
      </div>      
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Noke</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>The padlock that removes the need for keys</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">April 1, 2015</p>
      <p>The idea of carrying around several keys to get your into your property or belongings has seemed a little old fashioned for a while. Now for padlocks, at least, it is.</p>
      <p>Utah-based company FUZ Designs has developed a smart padlock called the Noke (pron. 'No Key'). The lock is Bluetooth<sup>&reg;</sup>-connected and can be paired with a compatible device such as a smartphone. You close the lock as you would a traditional device but unlocking is done automatically when the device's system detects a paired device in close proximity.</p>
      <p>Trust is essential in lock technology and the Noke utilizes Bluetooth 4.0 to provide the best protection on the market. It operates a 128-bit AES CCM encryption algorithm which has been added to FUZ Designs' own PKI technology and cryptographic key exchange protocol.</p>
      <p>To provide flexibility, digital keys can be shared between smartphones to provide shared access to your lock. This can happen across countries.</p>
      <p>In addition to the padlock, FUZ Designs has now created a hardened steel U-lock for bicycles. This operates the same ARM technology as the padlock but also features an alarm system with an accelerometer that detects movement. If a potential thief tries to cut the lock, the security system sounds an alarm that can be heard 50 meters away.</p>
      <p>So what happens if your phone dies or the Noke runs out of battery? Owners can use the 'Quick-Click' code. This is a set sequence of short and long clicks (similar to Morse code) that are tapped manually into the Noke to unlock it. The Noke has a notification light to alert users when the battery is low and can be replaced without the need for any tools. </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/MnUC9us9NLg?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>As the Noke runs off a lithium  polymer battery, power management and low-energy consumption were paramount  when deciding on the processor to be used inside the device. Fuz Designs chose  the nRF51822 SoC from Nordic Semiconductor which utilizes the ARM<sup>&reg;</sup>  Cortex<sup>&reg;</sup>-M0 processor as it provides energy-efficiency and low-power  consumption to enable the battery to last years of continual use.</p>
</div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.fuzdesigns.com/pages/noke" target="_blank">Fuz Designs</a></p>
        <img src="/innovation/assets/images/products/noke/fuz-designs-logo.jpg" alt="Fuz Designs logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-nordic-semiconductor-asa" target="_blank">Nordic Semiconductor</a></p>
        <img src="/innovation/assets/images/partner-logos/nordic-semiconductor-logo.jpg" class="logos image" />         
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m0.php" target="_blank">ARM Cortex-M0</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>