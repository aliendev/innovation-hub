<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - MobiCuff</title>

    <meta name="description" content="MobiCuff is an adapter that converts a blood pressure cuff into a smartphone blood pressure monitor. ">
    <meta name="keywords" content="ARM, innovation, innovation hub, MobiCuff, Blood pressure monitor, ARM health, ARM innovation, smart blood pressure, Smart health">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - MobiCuff" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/mobicuff.php" />
    <meta property="og:description" content="MobiCuff is an adapter that converts a blood pressure cuff into a smartphone blood pressure monitor. " />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/mCuff/mobi-cuff-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - MobiCuff" />
    <meta name="twitter:description" content="MobiCuff is an adapter that converts a blood pressure cuff into a smartphone blood pressure monitor. " />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/mCuff/mobi-cuff-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/mobicuff.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - MobiCuff">
    <meta itemprop="description" content="MobiCuff is an adapter that converts a blood pressure cuff into a smartphone blood pressure monitor. ">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/mCuff/mobi-cuff-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">MobiCuff</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/mCuff/mobi-cuff1.jpg" alt="MobiCuff"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/mCuff/mobi-cuff2.jpg" alt="MobiCuff"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/mCuff/mobi-cuff3.jpg" alt="MobiCuff"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>MobiCuff</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>The smarter way to measure blood pressure</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">May 1, 2015</p>
      <p>High blood pressure (hypertension) is estimated to kill nine million people a year according to the World Health Organization. It is a particular problem in developing world countries where many people are affected by associated illnesses such as heart disease, stroke and kidney failure. However, this is a problem that is extremely difficult to tackle, especially in regions where medical technology and training may be of a relatively poor standard. The main issue is the difficulty in spotting early signs of the disease as suffers show few external symptoms. For this reason, the disease is dubbed the ‘silent killer’ and this is where a new device called MobiCuff comes in.</p> 
<p>Developed by scientists from the Oxford Centre for Affordable Healthcare Technology, MobiCuff is a low-cost, connected device designed to accurately measure blood pressure at less than one sixth the cost of the cheapest existing devices. It utilizes a traditional cuff and air pump but the measurement process is guided by a smartphone app that interprets the data and sends to a patient’s electronically-held medical records.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/zZddasBYfRs?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The device contains a Silicon Labs EFM32TG210 32-bit microcontroller based on the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M3 processor. The industry-leading 32-bit Cortex-M3 processor was chosen to enable the computational performance needed while maintaining maximum energy-efficiency and low-power usage.</p> 
<p>The compact device can report pulse rates, systolic and diastolic blood pressures, has a test range of 30-260mmHg, and is accurate to within two per cent. As a smart device, companion smartphone apps will be made available to track your measurement history, remind you to take new readings, and most importantly, identify irregularities.</p> 
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.medyc.com/#mobicuff" target="_blank">Medyc</a></p>
        <img src="/innovation/assets/images/products/mCuff/medyc-logo.jpg" alt="Medyc logo" class="logos image"/>
        <br class="space">
        <img src="/innovation/assets/images/products/mCuff/mobi-cuff-logo.jpg" alt="MobiCuff logo" class="logos image"/>
        <br class="space">
        <img src="/innovation/assets/images/products/mCuff/caht-logo.jpg" alt="Caht+ logo" class="logos image"/>
        <br class="space">
        <img src="/innovation/assets/images/products/mCuff/safe-heart-logo.jpg" alt="Safe Heart logo" class="logos image"/>        
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-silicon-labs" target="_blank">Silicon Labs</a></p>
        <img src="/innovation/assets/images/partner-logos/silicon-labs-logo.jpg" alt="Silicon Labs logo" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php" target="_blank">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>