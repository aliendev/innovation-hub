<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - DICE+</title>

    <meta name="description" content="Dice+ is an interactive gaming device that connects to your tablet to provide an integrated gaming experience. You can throw away your old board games and enjoy games on your tablet with the interactivity of rolling a real die.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, Dice+, dice plus, smart dice, mobile game dice, tablet game dice, interactive dice, game technology, board games, interactive board games">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - DICE+" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/dice-plus.php" />
    <meta property="og:description" content="Dice+ is an interactive gaming device that connects to your tablet to provide an integrated gaming experience. You can throw away your old board games and enjoy games on your tablet with the interactivity of rolling a real die." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/dice-plus/DICEplus-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - DICE+" />
    <meta name="twitter:description" content="Dice+ is an interactive gaming device that connects to your tablet to provide an integrated gaming experience. You can throw away your old board games and enjoy games on your tablet with the interactivity of rolling a real die." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/dice-plus/DICEplus-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/dice-plus.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - DICE+">
    <meta itemprop="description" content="Dice+ is an interactive gaming device that connects to your tablet to provide an integrated gaming experience. You can throw away your old board games and enjoy games on your tablet with the interactivity of rolling a real die.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/dice-plus/DICEplus-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>



<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">DICE+</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    


<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="/innovation/assets/images/products/dice-plus/DICEplus2.jpg" alt="DICE+">
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dice-plus/DICEplus6.jpg" alt="DICE+">
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dice-plus/DICEplus4.jpg" alt="DICE+">
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dice-plus/DICEplus5.jpg" alt="DICE+">
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dice-plus/DICEplus3.jpg" alt="DICE+">
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>DICE+</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>DICE+ aims to roll board games into the future</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>With the increasing popularity of mobile and tablet games, traditional board games are becoming a thing of the past.  Modern gamers want instant action and guaranteed fun without broken parts and missing pieces lost over time. In fact, most of today's gamers haven't even heard of classic board games! Game Technologies believes the Dice+ can bridge the gap between generations, allowing players to play board games on their smart device with the interactivity of rolling a die.</p>
      <p>Tablets and mobiles have enabled classic board games such as Risk, Ticket to Ride and Monopoly to become interactive with rich graphics, colorful animations, combined with pass-and-play gameplay to allow enjoyment for the whole family. However, the main issue with these games is they lack the authenticity and fun of rolling a die that will dictate your fate in either taking over the world or ending up in jail. </p>
      <p>The DICE+ is a digital die that connects with a mobile device via a Bluetooth connection allowing players to have a level of interactivity that is not achieved with just a screen device.  </p>
      <p>Measuring one inch on each side with a soft-touch rubbery exterior, the DICE+ allows players to have up to 20 hours of continuous rolling while different colored numbers on each side illuminate to show each player when it is their turn to throw. </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/xDuq6hAtgFE?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Powered by a STM32 family microcontroller from STMicroelectronics utilizing the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup> M3 processor, the DICE+ features a built in accelerometer, thermometer, and magnetometer, meaning it is so smart it can tell when you are cheating to get the number you want to win! </p>
      <p>The accompanying DICE+ app allows users to download a whole suite of games, ranging from traditional board games such as backgammon, to educational games for children, thus allowing the whole family to take part and enjoy playing games. </p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://dicepl.us/" target="_blank">DICE+</a></p>
        <img src="/innovation/assets/images/products/dice-plus/dice-plus-logo.jpg" alt="DICE+ logo" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php" target="_blank">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>