<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - Dash &amp; Dot</title>

    <meta name="description" content="Dash & Dot have been designed to teach coding concepts to children aged between 5 and 12 through a play-based programming system combining stories, music and animation.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, Dash & Dot, programming for children, teach kids code, toy robots, children coding">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Dash & Dot" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/dash-n-dot.php" />
    <meta property="og:description" content="Dash & Dot have been designed to teach coding concepts to children aged between 5 and 12 through a play-based programming system combining stories, music and animation." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/dash-n-dot/dash-n-dot-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Dash & Dot" />
    <meta name="twitter:description" content="Dash & Dot have been designed to teach coding concepts to children aged between 5 and 12 through a play-based programming system combining stories, music and animation." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/dash-n-dot/dash-n-dot-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/dash-n-dot.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Dash & Dot">
    <meta itemprop="description" content="Dash & Dot have been designed to teach coding concepts to children aged between 5 and 12 through a play-based programming system combining stories, music and animation.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/dash-n-dot/dash-n-dot-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>



<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->



    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Dash and Dot</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    


<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot2.jpg" alt="Dash & Dot"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot1.jpg" alt="Dash & Dot at work"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot8.jpg" alt="Dash & Dot, app interface"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot5.jpg" alt="Dash & Dot, app interface"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot6.jpg" alt="Dash & Dot, kids"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/dash-n-dot/dash-n-dot7.jpg" alt="Dash & Dot, portable"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Dash &amp; Dot</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Meet Dash &amp; Dot, toy robots that teach kids code</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>According to Code.org, by 2020 there will be 1.4 million more computer science jobs than there will be people to fill them and yet less than 2.4% of graduates are studying computer science. Wonder Workshop aims to change this future lack of talent by introducing children to programming at a young age to inspire future careers in technology through the inventions of Dash and Dot.  </p>
      <p>Teaching coding to children is becoming ever more important in today’s society as an early understanding of programming assists the development of skills such as logic, which promotes the development in subjects such as math and robotics. </p>
      <p>Dash and Dot have been designed to teach coding concepts to children aged between five and 12 through a series of stories which are easy for children to understand. By using a tablet or touchscreen device, users can click on a series of illustrated icons to ‘program’, or direct the robots to execute a series of commands to foster a basic level of programing concepts for children. </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/MLKyBXUfTHQ?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The Bluetooth<sup>&reg;</sup> based remote control app simplifies arm, eye, gesture and wheel commands to reduce them to icon-based sequences. Children can program Dash to give a flower to a friend, play a song on toy xylophone or program Dot’s eyes to light up or make animal sounds when shaken. Both robots can be used together or separately and can do anything from basic movements and object detection to more complex instructions.</p>
      <p>The device features the nRF51822 SoC from Nordic Semiconductor and a N572 series microcontroller from Nuvoton. Both are powered by ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M0 processors to enable a wide range of actions; the robots include several sensors, including distance sensors, sound sensors (for voice commands), proximity sensors (to detect its distance from walls), beacon sensors (to sense other robots so that kids can program their robots to play with each other) and accelerometers. </p>
    </div>    
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://www.makewonder.com/robots/dashanddot" target="_blank">Wonder Workshop</a></p>
         <img src="/innovation/assets/images/products/dash-n-dot/wonder-works-logo.jpg" width="149" height="75" alt="Wonder Workshop logo" class="logos image"/></div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partners</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-nuvoton" target="_blank">Nuvoton</a></p>
        <img src="/innovation/assets/images/partner-logos/nuvoton-logo.jpg" class="logos image" />
        <br class="space">        
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-nordic-semiconductor-asa" target="_blank">Nordic Semiconductor</a></p>
        <img src="/innovation/assets/images/partner-logos/nordic-semiconductor-logo.jpg" class="logos image" />         
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m0.php" target="_blank">ARM Cortex-M0</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>