<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - AiR 3D Smart Glasses</title>

    <meta name="description" content="Atheer Labs’ AiR 3D smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces.">
    <meta name="keywords" content="arm, innovation, innovation hub, smart glasses, smart headwear, smart enterprise, innovative glasses, smart tech">

    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices -->
    <meta property="og:title" content="ARM | Innovation Hub - AiR 3D Smart Glasses" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/air-3d-smart-glasses.php" />
    <meta property="og:description" content="Atheer Labs AiR 3D smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/air-3D/air-3d-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug -->

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - AiR 3D Smart Glasses" />
    <meta name="twitter:description" content="Atheer Labs AiR 3D smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/air-3D/air-3d-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/air-3d-smart-glasses.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link -->

    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ -->
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api -->

    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - AiR 3D Smart Glasses">
    <meta itemprop="description" content="Atheer Labs AiR 3D smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/air-3D/air-3d-sm.jpg">

    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>

</head>

<body>


    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page -->

            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">AiR 3D Smart Glasses</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>


     </ul>
    </div>


<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>

  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/air-3D/air-3d-2.jpg" alt="AiR 3D Smart Glasses"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/air-3D/air-3d-3.jpg" alt="AiR 3D Smart Glasses"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/air-3D/air-3d-1.jpg" alt="AiR 3D Smart Glasses"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/air-3D/air-3d-4.jpg" alt="AiR 3D Smart Glasses"/>
        <div class="container">
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>-->
        </div>
      </div>
    </div>
    <!-- end carousel-inner -->

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a>
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->

  <div id="main-body-container" class="clearfix">
    <h1>AiR 3D Smart Glasses</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Enabling virtual environments that deliver real benefits</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?>
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">June 1, 2015</p>
      <p>Smart glasses have the potential to enhance all sorts of industries by complementing the physical world with a new information-rich virtual platform. Atheer Labs believe their smart glasses platform can change the way industries operate by enabling immersive interactive environments to increase productivity and efficiency in workforces.</p>

<p>Based in Mountain View, California, Atheer Labs has created a 3D augmented reality platform with gesture-based interactions for enterprise customers in the industrial, oil and gas and medical industries. Their platform consists of its AiR<sup>&trade;</sup> Smart Glasses and AiR OS, in addition to the AiR DK2 developer kit.</p>

<p>AiR provides users with accurate real-time information that will enable industry-specific advantages to companies. The device will be able to enable hands-free capabilities for field tech, assistance for customer service agents and access to patient data for on-the-go doctors.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/T0onzbGNJIQ?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Weighing less than 75 grams, AiR has a 65-degree field of view with 1048x785 resolution for each eye. The device uses dual 8-megapixel camera and a 3D depth sensor for sub-millimeter hand recognition accuracy, allowing users to physically control what is in front of them. The device connects to smartphones via micro-USB and can output 3D video and respond to gesture, voice and head motion commands.</p>
<p>The device contains the ARM<sup>&reg;</sup>-based Qualcomm<sup>&reg;</sup> Snapdragon<sup>&reg;</sup> 800 processor. The processor was chosen as it provides energy-efficient advanced video, camera and audio experiences for users.
</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://www.atheerlabs.com/" target="_blank">Atheer</a></p>
        <img src="/innovation/assets/images/products/air-3d/atheer-logo.jpg" alt="Atheer logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext">Qualcomm</p>
        <img src="/innovation/assets/images/partner-logos/qualcomm-logo.jpg" class="logos image" />
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext">ARM-based processor</p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container -->
<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/innovation/assets/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script>
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>
