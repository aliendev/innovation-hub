<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - GlassUp</title>

    <meta name="description" content="">
    <meta name="keywords" content="">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="fb:app_id" content="" />
    <meta property="og:type" content="" />
    <meta property="og:locale" content="" />
    <meta property="article:author" content="" />
    <meta property="article:publisher" content="" />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="The Name or Title Here">
    <meta itemprop="description" content="This is the page description">
    <meta itemprop="image" content="http://www.example.com/image.jpg">
    

    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>


<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">GlassUp</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="/innovation/assets/images/products/glass-up/GlassUp5.jpg" alt="GlassUp"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/glass-up/GlassUp1.jpg"  alt="GlassUp"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/glass-up/GlassUp2.jpg" alt="GlassUp"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/glass-up/GlassUp3.jpg" alt="GlassUp"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/glass-up/GlassUp4.jpg" alt="GlassUp"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>GlassUp</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Augmented reality glasses become fashionable </h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">January 20, 2015</p>
      <p>Have you ever thought how cool it would be to control your apps direct from your glasses? But instead of looking like a cyborg, you could sport a stylish pair of Italian frames that made you look uber-trendy?  Well, GlassUp has come up with the perfect solution, an always-visible smart display pair of glasses that have been designed for comfort and style.
</p>
      <p>GlassUp is a second screen for your smartphone on your glasses. Incoming emails, text messages, tweets, Facebook updates and news bulletins are projected in front of the user while other apps which utilize GPS or translation software for when you are abroad are available to download. Data transferred to GlassUp is simply determined by the apps you choose to download.        </p>
      <p>The streamlined GlassUp doesn’t feature a camera, heavy-duty electronics and has a battery life of 150 hours on standby or eight hours of constant use. The lightweight design weighs only 40 grams more than a traditional pair of glasses and as GlassUp is in the form of high-fashion eyeglasses, prescription versions are planned which will allow users to place a polycarbonate clip-on to enable perfect vision with augmented reality.        </p>
      <p>Everyone from drivers and runners to doctors, public speakers and the hearing impaired can get access information via the glasses without being forced to look away!        </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <object width="1280" height="720">
            <param name="movie" value="http://www.youtube.com/v/os6bqbTo4Qs?rel=0"></param>
            <param name="allowFullScreen" value="true"></param>
            <param name="allowscriptaccess" value="always"></param>
            <param name="wmode" value="transparent"></param>
            <embed src="http://www.youtube.com/v/os6bqbTo4Qs?rel=0" type="application/x-shockwave-flash" width="1280" height="720" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"></embed>
          </object>
        </div>
      </div>
      <p>GlassUp is built on an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M4 and works via a Bluetooth<sup>&trade;</sup> connection with a paired smartphone, meaning you don’t have to delve into your pocket every time you hear a message ping!        </p>
      <p>Getting non-techie people to sport glasses isn’t easy, especially augmented-reality ones, but by taking the geek out of the design and making them a fashion accessory might just give GlassUp the edge! </p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.glassup.net/" target="_blank">GlassUp</a></p>
        <img src="/innovation/assets/images/products/glass-up/glass-up-logo.jpg" width="267" height="75" alt="GlassUp logo" class="logos image"/></div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext">XXX</p>
        <img src="NEED PARTNER LOGO HERE" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m4-processor.php" target="_blank">ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M4</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>