<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - Automatic</title>

    <meta name="description" content="The Automatic from Automatic Labs is a smartphone enabled driving assistant, it provides you with driving feedback and helps diagnose engine codes to become a safer driver.">
    <meta name="keywords" content="arm, innovation,  innovation hub, automatic, OBD-II car, driving assistance, on-board diagnostics, automatic link, vehicle diagnostics, smarter driver, driving feedback, smartphone car, intelligent car">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Automatic" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/automatic.php" />
    <meta property="og:description" content="Nobody likes being critiqued on their driving ability, however it’s important to understand how you can improve your driving style, remove bad habits and save money in the process. Automatic Labs believe they have the answer to this with the Automatic." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/automatic/automatic-logo.png" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Automatic" />
    <meta name="twitter:description" content="Nobody likes being critiqued on their driving ability, however it’s important to understand how you can improve your driving style, remove bad habits and save money in the process. Automatic Labs believe they have the answer to this with the Automatic." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/automatic/automatic-logo.png" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/automatic.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Automatic">
    <meta itemprop="description" content="Nobody likes being critiqued on their driving ability, however it’s important to understand how you can improve your driving style, remove bad habits and save money in the process. Automatic Labs believe they have the answer to this with the Automatic.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/automatic/automatic-logo.png">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>


<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Automatic</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="/innovation/assets/images/products/automatic/Automatic1.jpg" alt="Automatic"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/automatic/Automatic2.jpg" alt="Automatic"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/automatic/Automatic3.jpg" alt="Automatic"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/automatic/Automatic4.jpg" alt="Automatic"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Automatic</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>It's like having a back-seat driver, but less annoying</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>"You're going too fast." "You missed the turn!" "Do you need to brake so hard?" Any of this sound familiar? If so, the chances are you are sharing your journey with a backseat driver. Nobody likes being critiqued on their driving ability by a passenger, however it is important to understand how you can improve your driving style, remove bad habits and save money in the process. Automatic Labs believe it has the answer to this with their product, Automatic. </p>
      <p>Since 1996, every car that's been produced in the United States has an OBD-II port. While most of us have no idea where this port is or what it does, mechanics and car dealers use it daily by connecting a computer to diagnose vehicle diagnostics and automotive issues. Automatic has been designed to slot into this port and capture car data to send to your synchronized smartphone via Bluetooth<sup>&reg;</sup> low energy.  </p>
      <p>With a recent study stating that 14 percent of drivers were involved in an accident or near miss because they were distracted by a backseat driver, the importance of Automatic cannot be underestimated. Rather than shout at you or pretend to push down hard on an invisible passenger brake, Automatic makes clear sounds to alert the driver when their driving may be hazardous while its built-in accelerometer detects if you have slammed the brakes, accelerated aggressively or cruised consistently at speeds well above 70MPH. Drivers can learn how safe they are on the road by viewing a weekly 'Drive Score' via the smartphone app. </p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/_AyXNeRbpRk?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Powered by an ARM<sup>&reg;</sup>  Cortex<sup>&reg;</sup>-M3 processor on a STM32  family  MCU from STMicroelectronics,  the Automatic   is  far more  sophisticated than a  mechanic's tool.  The device  monitors all car activity  so you  can see how much  each  leg of your journey  costs, how much time each  journey takes  and how  many  miles you  have driven,  allowing you to calculate exactly how much to charge friends for lifts!</p>
      <p>Automatic  utilizes the smartphone's GPS when connected to track all journeys and record where you have parked your vehicle (which is brilliant for those of us who forget where we park!). Interpretation of engine light/warning  indicators is available on the app allowing you to solve and clear issues without  the need of a costly mechanic. </p>
<p class="source">* http://magazine.nationwide.com/are-you-backseat-driver</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://www.automatic.com/" target="_blank">Automatic Labs</a></p>
        <img src="/innovation/assets/images/products/automatic/automatic-logo.png" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php" target="_blank">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>