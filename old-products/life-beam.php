<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - LifeBEAM Smart Headwear</title>

    <meta name="description" content="LifeBEAM has created a range of smart headwear that utilize biometric sensor technology to enable athletes and sports enthusiasts to obtain feedback on their performances">
    <meta name="keywords" content="arm, innovation, innovation hub, wearables, smart headwear, smart cycling, cycling hat, smart visor, smart cap, smart hat">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - LifeBEAM Smart Headwear" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/life-beam.php" />
    <meta property="og:description" content="LifeBEAM has created a range of smart headwear that utilize biometric sensor technology to enable athletes and sports enthusiasts to obtain feedback on their performances" />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/life-beam/lifebeam-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - LifeBEAM Smart Headwear" />
    <meta name="twitter:description" content="LifeBEAM has created a range of smart headwear that utilize biometric sensor technology to enable athletes and sports enthusiasts to obtain feedback on their performances" />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/life-beam/lifebeam-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/life-beam.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - LifeBEAM Smart Headwear">
    <meta itemprop="description" content="LifeBEAM has created a range of smart headwear that utilize biometric sensor technology to enable athletes and sports enthusiasts to obtain feedback on their performances">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/life-beam/lifebeam-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">LifeBEAM Smart Headwear</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/life-beam/lifebeam2.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam3.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam6.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam5.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam4.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam8.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/life-beam/lifebeam7.jpg" alt="LifeBEAM Smart Headwear"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
        <li data-target="#myCarousel" data-slide-to="5"></li>
        <li data-target="#myCarousel" data-slide-to="6"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>LifeBEAM Smart Headwear</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Your exercise routine just got smarter</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">June 1, 2015</p>
      <p>For sports enthusiasts who really want to understand how their body is performing under stress a biometric chest strap has been a necessary but not entirely comfortable accessory. But now LifeBEAM believe they have the solution to this with a new range of comfort-focused sporting wearables.</p>
<p>LifeBEAM, developers of bio-sensing technology to monitor the vital performance signs of pilots and astronauts, has created a range of smart headwear that utilize biometric sensor technology to enable athletes and sports enthusiasts to obtain feedback on their performances.</p> 
<p>The range of wearable products comprises of the Smart Helmet, the Smart Hat and the Smart Visor. Each product features optical sensors that record EKG-accurate heart rate monitoring, cadence/steps measurement and calorie consumption. The data obtained can be sent wirelessly any compatible smartphone, sports watch or cycling computer via Bluetooth<sup>&reg;</sup> 4.0 and ANT+<sup>&trade;</sup>.
</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/BOgImWCGL08?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The devices are designed to be comfortable and unobtrusive, delivering up to 20 hours of battery life with continuous use and is all weather compatible. To maximize the user experience, the Smart Visor and Smart Helmet feature sound and an LED user interface to give unified and insightful feedback.</p>  
<p>Each product in the LifeBEAM range features an STM32 family SoC that utilizes the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M3 processor. LifeBEAM chose this processor as it provides low-power consumption with high-performance necessary to maximize the battery life of each wearable.</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://life-beam.com/" target="_blank">LifeBEAM</a></p>
        <img src="/innovation/assets/images/products/life-beam/lifebeam-logo.jpg" alt="LifeBEAM logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php" target="_blank">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>