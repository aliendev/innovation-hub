<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Kairos Smartwatches</title>

    <meta name="description" content="Kairos' bridge the gap between old and new with their range of hybrid mechanical smartwatches.">
    <meta name="keywords" content="arm, innovation, innovation hub, smart watch, smart devices, smartwatch, fashionable watches, hybrid watch, innovative watch">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Kairos Smartwatches" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/kairos.php" />
    <meta property="og:description" content="Kairos' bridge the gap between old and new with their range of hybrid mechanical smartwatches." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/kairos/kairos-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Kairos Smartwatches" />
    <meta name="twitter:description" content="Kairos' bridge the gap between old and new with their range of hybrid mechanical smartwatches." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/kairos/kairos-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/kairos.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Kairos Smartwatches">
    <meta itemprop="description" content="Kairos' bridge the gap between old and new with their range of hybrid mechanical smartwatches.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/kairos/kairos-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Kairos Smartwatches</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/kairos/kairos1.jpg" alt="Kairos Smartwatches"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/kairos/kairos2.jpg" alt="Kairos Smartwatches"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/kairos/kairos3.jpg" alt="Kairos Smartwatches"/>>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/kairos/kairos4.jpg" alt="Kairos Smartwatches"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Kairos Smartwatches</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Turning the traditional watch into a smart watch</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">June 1, 2015</p>
      <p>Smart watches are beginning to turn the corner from nouveaux fashion accessory to must-have device yet some still hanker for a design that pays homage to a mechanical time piece past. Here’s where Kairos come in as they are bridging the gap between old and new with a range of hybrid mechanical smartwatches.</p> 
<p>Kairos, a California-based company, has developed a range of hybrid mechanical smartwatches that superimpose notifications and other graphics over the mechanical parts.</p> 
<p>Their hybrid smartwatches come in two forms: MSW and SSW. The MSW variant uses Miyota Japanese movements, while the SSW uses Soprod Swiss movements to cater for all markets.  Each device comes in three form factors: a traditional watch with zero technology, a smart version with an OLED (organic light-cemitting diode) overlay on the glass, and the T-Band edition that allows users to turn their traditional watches into a smart watch using the smart strap.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/ndycU_dUHNQ?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>Each device uses Kairos OS and connects to smartphones via Bluetooth<sup>&reg;</sup> technology. Users are able to see notifications such as text messages, incoming calls, emails alerts and social media messages on the OLED screen. The device can double-up as a remote control for smartphones and tablets and also provides fitness and sleep analysis through its optical and GSR sensors (T-Band HD).</p> 
<p>The Kairos family of smartwatches features a STM32F4 SoC that utilizes the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M4 processor. Kairos chose this SoC due to its low power consumption that enable the battery life to be extended to almost a week.</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="https://kairoswatches.com/" target="_blank">Kairos</a></p>
        <img src="/innovation/assets/images/products/kairos/kairos-logo.jpg" alt="Kairos logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m4-processor.php" target="_blank">ARM Cortex-M4</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>