<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - Lively</title>

    <meta name="description" content="Lively respects the privacy of older adults by measuring healthy living patterns and giving family members a way to quickly realize when help may be needed. Taking medication on time? Eating regularly? Being as active as possible? When something is amiss, Lively notifies loved ones to help.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, lively, Live!y, smart caregiving, monitor seniors in their home, home monitor technology, smart monitor system">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Lively" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/lively.php" />
    <meta property="og:description" content="Lively respects the privacy of older adults by measuring healthy living patterns and giving family members a way to quickly realize when help may be needed. Taking medication on time? Eating regularly? Being as active as possible? When something is amiss, Lively notifies loved ones to help." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/lively/lively-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Lively" />
    <meta name="twitter:description" content="Lively respects the privacy of older adults by measuring healthy living patterns and giving family members a way to quickly realize when help may be needed. Taking medication on time? Eating regularly? Being as active as possible? When something is amiss, Lively notifies loved ones to help." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/lively/lively-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/lively.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Lively">
    <meta itemprop="description" content="Lively respects the privacy of older adults by measuring healthy living patterns and giving family members a way to quickly realize when help may be needed. Taking medication on time? Eating regularly? Being as active as possible? When something is amiss, Lively notifies loved ones to help.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/lively/lively-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    


</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Lively</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/lively/Lively2.jpg" alt="Lively"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lively/Lively3.jpg" alt="Lively"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lively/Lively1.jpg" alt="Lively"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lively/Lively4.jpg" alt="Lively"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/lively/Lively5.jpg" alt="Lively"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>Lively</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Caregiving goes digital</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>As the global population aged 65 and over is expected to reach  715 million in 2020 and 1.5 billion in 2050, there will be an increasing demand for healthcare and caregivers to support and look after the elderly population. This demand, combined with senior adults seeking independence, security and the comfort of their own home means there will be a heavy financial cost for governments to provide healthcare.  </p>
      <p>The Lively system alleviates this pressure with its digital hub and sensors, which monitor and record the actions of older adults in their own home. Lively sensors, attached to important items such as the shower door, medicine box and the refrigerator, all send usage data to the Lively Hub.  </p>
      <p>The data produced can be securely accessed by family members and carers via the web or smartphone app to view and understand whether the senior person is taking their medicine, eating regularly and drinking properly while a key fob featuring a Bluetooth<sup>&reg;</sup> low energy transmitter helps determine if the person being monitored has left the house. This allows the family  (usually the adult child) of the older adult to have a non-invasive overview of the health and wellbeing while providing the homeowner with a healthy and independent lifestyle. </p>
	<div id="containingBlock">
        <div class="videoWrapper">
         <iframe width="1280" height="720" src="https://www.youtube.com/embed/0Rcj6JBpM3E?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe> 
        </div>
      </div>
      <p> The Lively system utilizes the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M0 processor on an nRF51822 SoC from Nordic Semiconductor for each sensor while the Lively hub is powered by an STM32 family MCU from STMicroelectronics powered by the ARM Cortex-M4 processor. </p>
      <p>To ensure compatibility in all homes, Lively has been designed to be easily installed, with the base station featuring embedded cellular connectivity so users do not have to link it to a Wi-Fi network or internet connection. </p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.mylively.com/" target="_blank">Lively</a></p>
        <img src="/innovation/assets/images/products/lively/lively-logo.jpg" alt="August logo" class="logos image"/>
      </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partners</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-nordic-semiconductor-asa" target="_blank">Nordic Semiconductor</a></p>
        <img src="/innovation/assets/images/partner-logos/nordic-semiconductor-logo.jpg" class="logos image" /> 
        <br class="space">        
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-stmicroelectronics" target="_blank">STMicroelectronics</a></p>
        <img src="/innovation/assets/images/partner-logos/stmicro-logo.png" class="logos image" /> 
      </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m0.php" target="_blank">ARM Cortex-M0</a></p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m4-processor.php" target="_blank">ARM Cortex-M4</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>