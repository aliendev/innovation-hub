<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0">
<title>ARM | Innovation Hub - Simprints</title>

    <meta name="description" content="SimPrints is a pocket-sized fingerprint scanner that digitally links a person’s fingerprints to their health records.">
    <meta name="keywords" content="arm, innovation, innovation hub, SimPrints, fingerprint scanner, Bluetooth fingerprint, medical record scanner, medical fingerprints, biometric fingerprints">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - Simprints" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/simprints.php" />
    <meta property="og:description" content="SimPrints is a pocket-sized fingerprint scanner that digitally links a person’s fingerprints to their health records." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/simprints/simprints-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - Simprints" />
    <meta name="twitter:description" content="SimPrints is a pocket-sized fingerprint scanner that digitally links a person’s fingerprints to their health records." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/simprints/simprints-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/simprints.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - Simprints">
    <meta itemprop="description" content="SimPrints is a pocket-sized fingerprint scanner that digitally links a person’s fingerprints to their health records.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/simprints/simprints-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>




<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">Simprints</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    

    
<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item active"><img src="/innovation/assets/images/products/simprints/simprints4.jpg" alt="Simprints"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/simprints/simprints1.jpg" alt="Simprints"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/simprints/simprints2.jpg" alt="Simprints"/>
        <div class="container"> </div>
      </div>
      <div class="item"><img src="/innovation/assets/images/products/simprints/simprints3.jpg" alt="Simprints"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> 
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>SimPrints</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Lifesaving information at your fingertips</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">May 1, 2015</p>
      <p>The keeping of accurate medical records linked to the correct person is at the heart of providing reliable healthcare but in many developing countries this remains a challenge. A company based in Cambridge called SimPrints is set on resolving this with a biometric scanner that digitally links a person's fingerprints to their health records. The scanner offers many advantages in terms of identification confidence but it also means medical records can be kept on-line with a reliable backup that is updated electronically.</p>
<p>Health workers out in the field are able to quickly identify patients as the scanner syncs with their smartphone and then seamlessly obtains their records. When internet connectivity is poor, the device can be placed in an 'offline mode' where the SimPrints system accesses a previously downloaded database with newly obtained patient data uploaded when connectivity is regained.</p>
<p>The system also gets around the issues in many developing nations where people are only known by common community names and with unknown dates of birth. Also, any records that do exist are paper-based and therefore prone to being lost or damaged.</p>
<div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/LGMbDt64VuI?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>SimPrints is further enhancing the security of its technology by storing all patients' biometric data with time stamps and GPS coordinates to record where each patient has been assessed. Fingerprint scans are not transmitted and all records are RSA encrypted with scanners connecting wirelessly to a Bluetooth<sup>&reg;</sup> 2.0 compatible phone.</p> 
      <p>The pocket-sized scanner has been shaped to fit in the palm of a hand and is durable enough to be carried by emergency medical teams in the toughest conditions. SimPrints is currently prototyping the device with an ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M processor due to its energy-efficiency and ease of use to enable developers to meet the needs of tomorrow’s smart and connected embedded applications.</p> 
<p>Future SimPrints API with allow healthcare providers to integrate the technology into the majority of mHealth applications to enhance existing services with accurate patient identification and CHW monitoring functionalities. Future evolutions of the device will utilize SMS messaging to enable compatibility with J2ME and basic phones which are still used in developing countries.</p>
<p class="source">* Photo credit: Cambridge News</p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.simprints.com/" target="_blank">SimPrints</a></p>
        <img src="/innovation/assets/images/products/simprints/simprints-logo.jpg" alt="Simprints logo" class="logos image"/>
      </div>
      <!--<div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-texas-instruments" target="_blank">Partner name here</a></p>
        <img src="/innovation/assets/images/partner-logos/xxx.jpg" class="logos image" /> 
      </div>-->
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/index.php" target="_blank">ARM Cortex-M</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>