<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta charset="utf-8">
<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0">
<title>ARM | Innovation Hub - gTar</title>

    <meta name="description" content="gTar is a fully digital guitar that enables users to play music quickly and easily with the help of LEDs and a docked smartphone. Budding musicians simply select a song via the smartphone app and the device's multi-touch LED fret board lights up to teach you how to play your favorite songs correctly.">
    <meta name="keywords" content="arm, innovation, hub, innovation hub, gTar, smart guitar, learn guitar, guitar technology">
    
    <!-- Facebook Meta Ref: https://developers.facebook.com/docs/sharing/best-practices --> 
    <meta property="og:title" content="ARM | Innovation Hub - gTar" />
    <meta property="og:site_name" content="ARM | Innovation Hub" />
    <meta property="og:url" content="http://www.arm.com/innovation/products/gTar.php" />
    <meta property="og:description" content="gTar is a fully digital guitar that enables users to play music quickly and easily with the help of LEDs and a docked smartphone. Budding musicians simply select a song via the smartphone app and the device's multi-touch LED fret board lights up to teach you how to play your favorite songs correctly." />
    <meta property="og:image" content="http://www.arm.com/innovation/assets/images/products/gTar/gTar-sm.jpg" />
    <!-- meta property="fb:app_id" content="" / -->
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="article:author" content="ARM Ltd." />
    <meta property="article:publisher" content="ARM Ltd." />
    <!-- Validator/Debugger: https://developers.facebook.com/tools/debug --> 

    <!-- Twitter Card Ref: https://dev.twitter.com/cards/types/summary --> 
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="Twitter @ARMCommunity" />
    <meta name="twitter:title" content="ARM | Innovation Hub - gTar" />
    <meta name="twitter:description" content="gTar is a fully digital guitar that enables users to play music quickly and easily with the help of LEDs and a docked smartphone. Budding musicians simply select a song via the smartphone app and the device's multi-touch LED fret board lights up to teach you how to play your favorite songs correctly." />
    <meta name="twitter:image" content="http://www.arm.com/innovation/assets/images/products/gTar/gTar-sm.jpg" />
    <meta name="twitter:url" content="http://www.arm.com/innovation/products/gTar.php" />
    <!-- when done and live, validate it. The validation link is in the above mentioned ref link --> 
    
    <!-- Pintrest seems to also use the Facebook's open graph stuff + a few more for extra benifits, ref: https://developers.pinterest.com/rich_pins_article/ --> 
    <!-- LinkedIn also uses OG, Ref: https://developer.linkedin.com/documents/share-api --> 
    
    <!-- G+ recommends the use of Schema.org markup, but will also use the OG data as well, ref: https://developers.google.com/+/web/snippet/, http://moz.com/blog/meta-data-templates-123 -->
    <meta itemprop="name" content="ARM | Innovation Hub - gTar">
    <meta itemprop="description" content="gTar is a fully digital guitar that enables users to play music quickly and easily with the help of LEDs and a docked smartphone. Budding musicians simply select a song via the smartphone app and the device's multi-touch LED fret board lights up to teach you how to play your favorite songs correctly.">
    <meta itemprop="image" content="http://www.arm.com/innovation/assets/images/products/gTar/gTar-sm.jpg">
    
    <script src="/innovation/assets/js/jquery.min.1.11.0.js"></script>



<link rel="stylesheet" href="/innovation/assets/css/home.css">
<link rel="stylesheet" href="/innovation/assets/css/product.css">
<!--[if lt IE 9]>
        <link rel="stylesheet" type="text/css" href="/innovation/assets/css/ie8.css" />
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


    <script language="JavaScript" type="text/javascript" src="/js/jquery.arm.js"></script>
    <script src='https://assets.adobedtm.com/cd32341bd8f641f183f6259cc9c47b3d9e84454d/satelliteLib-180b04bdc59d22e053f5a854ffaa3e7c4506db19.js'></script>    

</head>

<body>
    
    
    <div id="breadcrumbs">
        <span>You are here:</span>
        <ul>
        <li class="first"><a href="/index.php" title="home">Home</a></li>
            <!-- output php variable with breadcrumb string from parent page --> 
            
            <li><a href="/innovation/index.php" class="noline">Innovation Hub</a></li>
            <li><a href="" class="noline">gTar</a></li>
            <div id="pageType" class="hideAdobe">Innovation Hub</div>
            
            
     </ul>
    </div>    


<div id="primaryContainer" class="clearfix">
  <?php include("../assets/inc/global_header.php"); ?>
  
  <!-- Carousel
    ================================================== -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
      <div class="item"> <img src="/innovation/assets/images/products/gTar/gTar4.jpg" alt="gTar"/>
        <div class="container"> 
          <!--<div class="carousel-caption">
              <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
            </div>--> 
        </div>
      </div>   
      <div class="item active"> <img src="/innovation/assets/images/products/gTar/gTar1.jpg" alt="gTar"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/gTar/gTar2.jpg" alt="gTar"/>
        <div class="container"> </div>
      </div>
      <div class="item"> <img src="/innovation/assets/images/products/gTar/gTar3.jpg" alt="gTar"/>
        <div class="container"> </div>
      </div>
    </div>
    <!-- end carousel-inner --> 
    
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <img id="carousel-left-img" src="/innovation/assets/images/site/carousel-left-img.png" class="image" /> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <img id="carousel-right-img" src="/innovation/assets/images/site/carousel-right-img.png" class="image" /> <span class="sr-only">Next</span> </a> 
    <!-- controller box -->
    <div id="controller-box" class="clearfix">
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
    </div>
  </div>
  <!-- /.carousel -->
  
  <div id="main-body-container" class="clearfix">
    <h1>gTar</h1>
    <div id="posDiv" style="position:relative;">
      <div id="heading-left">
        <h2>Party like a rockstar!</h2>
      </div>
      <?php include_once("../assets/inc/share-this.product.php") ?> 
    </div><!-- end posDiv -->
    <hr>
    <div id="left-side" class="clearfix">
      <p class="date">February 20, 2015</p>
      <p>If you fancy yourself as a bit of a Jimi Hendrix and want to bang out tunes that will make grown men cry but you can't play the guitar, help is at hand.  The gTar could well be the answer to your musical ambitions. </p>
      <p>Described as "the first guitar that anybody can play," the full-size instrument features a phone dock and back-lit fretboard. Budding musicians simply select the songs to play along to via the gTar smartphone app and the device's multi-touch LED fretboard lights up to teach you how to play your favorite songs correctly.</p>
      <div id="containingBlock">
        <div class="videoWrapper">
          <iframe width="1280" height="720" src="https://www.youtube.com/embed/sb1wfD6Anww?wmode=transparent" frameborder="0" allowfullscreen wmode="opaque"></iframe>
        </div>
      </div>
      <p>The gTar features Texas Instruments' Stellaris<sup>&reg;</sup> LM3S3748 SoC based on the ARM<sup>&reg;</sup> Cortex<sup>&reg;</sup>-M3 processor. Budding DJs will be interested to know that gTar is a completely independent and fully compatible USB-MIDI device, even without a smartphone inserted, so you can link live sequencer software to mix, re-arrange and master tracks. </p>
      <p>The gTar learning system is so simple its founders claim it will have you playing to your friends in 15 minutes, which is just about as long as it will take them to light the camp fire. Kiss goodbye to spending hours alone trying to work out simple riffs like 'Smoke on the Water' by Deep Purple, this is the fast track to guitar stardom that will have you hooked from the outset. </p>
    </div>
    <div id="right-side" class="clearfix">
      <div id="vendor" class="right-box clearfix">
        <p class="section-title">Vendor</p>
        <p class="subtext"><a href="http://www.incidentgtar.com/" target="_blank">Incident</a></p>
        <img src="/innovation/assets/images/products/gTar/incident-logo.jpg" width="243" height="75" alt="Incident logo" class="logos image"/> </div>
      <div class="divider clearfix"></div>
      <div id="partner" class="right-box clearfix">
        <p class="section-title">ARM Partner</p>
        <p class="subtext"><a href="http://community.arm.com/community/arm-partner-directory/partner-texas-instruments" target="_blank">Texas Instruments</a></p>
        <img src="/innovation/assets/images/partner-logos/texas-instruments-logo.jpg" width="180" height="42" alt="Texas Instruments logo" class="logos image"/> </div>
      <div class="divider clearfix"></div>
      <div id="featured-tech" class="right-box clearfix">
        <p class="section-title">Featured ARM Technology</p>
        <p class="subtext"><a href="http://www.arm.com/products/processors/cortex-m/cortex-m3.php">ARM Cortex-M3</a></p>
      </div>
    </div>
  </div>
  <?php include("../assets/inc/global_footer.php"); ?>
</div>

<!-- end primary container --> 
<!-- Bootstrap core JavaScript
    ================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="/innovation/assets/js/bootstrap.min.js"></script> 
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug --> 
<script src="/innovation/assets/js/ie10-viewport-bug-workaround.js"></script> 
<script type="text/javascript">
    // toggles share this menu
    $(".clickme").click(function () {
        $("#share-this-container").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
</script>


    <?php include_once("../assets/inc/analytics_tracking.php") ?>


</body>
</html>