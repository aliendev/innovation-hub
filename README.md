# Innovation Hub

![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Using Innovation Hub project as an example plain HTML site [using GitLab Pages](http://aliendev.gitlab.io/innovation-hub/).

The project was originally created for a past position in 2015. The project was a section of a website that was designed to be completely seperated from the rest of the site. The section highlighted end user proudcts that utalized the companies various types of technologies.

We originally built the section of the site using raw HTML, CSS, and JS. I then utalized some PHP to give some templating to this project. Upon putting this project together for my portfolio, I decided to utalize GitLab pages and will swich over some of the technologies and frameworks to allow GitLab to give me some power there. I'll be using Jekyll and SASS going forward, while switching everything over to using Bootstrap 4 with FontAwesome.


---


https://gitlab.com/aliendev/innovation-hub



<!-- START doctoc generated TOC please keep comment here to allow auto update -->
