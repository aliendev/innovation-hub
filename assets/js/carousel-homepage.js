$(document).ready(function() {


    $("li.technology-menu").hover( 
        function() 
        {
            $(this).find("ul.xcorp-technology-inside").slideDown(420);
            return false;
        }, 
        function() 
        {
            $(this).find("ul.xcorp-technology-inside").slideUp(240);
            return false;
        }
    );

    $("li.partner-menu").hover( 
        function() 
        {
            $(this).find("ul.xcorp-partner-inside").slideDown(420);
            return false;
        }, 
        function() 
        {
            $(this).find("ul.xcorp-partner-inside").slideUp(240);
            return false;
        }
    );




    if ( $( window ).width() < 801 && $( window ).width() > 550 ) 
    {
        $('#product-info-3').css('display','none');
        $('#product-info-4').css('display','none');
    }
    else if ( $( window ).width() < 551 )
    {
        $('#product-info-2').css('display','none');
        $('#product-info-3').css('display','none');
        $('#product-info-4').css('display','none');
    }
    else 
    {
        $('#product-info-1').css('display','block');
        $('#product-info-2').css('display','block');
        $('#product-info-3').css('display','block');
        $('#product-info-4').css('display','block');
    }

    
    
    $(window).resize(function() {
        if ($(window).width() < 801 && $(window).width() > 550) {
            if ($('#carousel-controls')
                .find('div.cycle-pager-active').attr('id') === 'product-info-1' || 
                $('#carousel-controls').find('div.cycle-pager-active')
                        .attr('id') === 'product-info-2')
            {
                $('#product-info-1').css('display','block');
                $('#product-info-2').css('display','block');
                $('#product-info-3').css('display','none');
                $('#product-info-4').css('display','none');
            }
            else if ($('#carousel-controls')
                .find('div.cycle-pager-active').attr('id') === 'product-info-3' || 
                $('#carousel-controls').find('div.cycle-pager-active')
                    .attr('id') === 'product-info-4')
            {
                $('#product-info-1').css('display','none');
                $('#product-info-2').css('display','none');
                $('#product-info-3').css('display','block');
                $('#product-info-4').css('display','block');
            }
        }
        else if ( $( window ).width() < 551 )
        {
            if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-1')
            {
                $('#product-info-1').css('display','block');
                $('#product-info-2').css('display','none');
                $('#product-info-3').css('display','none');
                $('#product-info-4').css('display','none');
            }
            else if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-2')
            {
                $('#product-info-1').css('display','none');
                $('#product-info-2').css('display','block');
                $('#product-info-3').css('display','none');
                $('#product-info-4').css('display','none');
            }
            else if ($('#carousel-controls')
                .find('div.cycle-pager-active').attr('id') === 'product-info-3')
            {
                $('#product-info-1').css('display','none');
                $('#product-info-2').css('display','none');
                $('#product-info-3').css('display','block');
                $('#product-info-4').css('display','none');
            }
            else if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-4')
            {
                $('#product-info-1').css('display','none');
                $('#product-info-2').css('display','none');
                $('#product-info-3').css('display','none');
                $('#product-info-4').css('display','block');
            }
        }
        else 
        {
            $('#product-info-1').css('display','block');
            $('#product-info-2').css('display','block');
            $('#product-info-3').css('display','block');
            $('#product-info-4').css('display','block');
    }

    

});




$( '#home-page-carousel' ).on( 'cycle-update-view', function(event, optionHash) {
    // your event handler code here
    // argument opts is the slideshow's option hash
    //alert(JSON.stringify($(optionHash)));
    
   if ($(window).width() < 801 && $(window).width() > 550) {
        if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-1' || 
                $('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-2')
        {
            $('#product-info-1').css('display','block');
            $('#product-info-2').css('display','block');
            $('#product-info-3').css('display','none');
            $('#product-info-4').css('display','none');
        }
        else if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-3' || 
                $('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-4')
        {
            $('#product-info-1').css('display','none');
            $('#product-info-2').css('display','none');
            $('#product-info-3').css('display','block');
            $('#product-info-4').css('display','block');
        }
    }
    else if ( $( window ).width() < 551 )
    {
        if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-1')
        {
            $('#product-info-1').css('display','block');
            $('#product-info-2').css('display','none');
            $('#product-info-3').css('display','none');
            $('#product-info-4').css('display','none');
        }
        else if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-2')
        {
            $('#product-info-1').css('display','none');
            $('#product-info-2').css('display','block');
            $('#product-info-3').css('display','none');
            $('#product-info-4').css('display','none');
        }
        else if ($('#carousel-controls')
            .find('div.cycle-pager-active').attr('id') === 'product-info-3')
        {
            $('#product-info-1').css('display','none');
            $('#product-info-2').css('display','none');
            $('#product-info-3').css('display','block');
            $('#product-info-4').css('display','none');
        }
        else if ($('#carousel-controls').find('div.cycle-pager-active').attr('id') === 'product-info-4')
        {
            $('#product-info-1').css('display','none');
            $('#product-info-2').css('display','none');
            $('#product-info-3').css('display','none');
            $('#product-info-4').css('display','block');
        }
    }
 
    
});



});
