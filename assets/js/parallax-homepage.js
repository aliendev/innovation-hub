$(document).ready(function() {
    var pluginName = 'scrolly',
        defaults = {
            bgParallax: false
        },
        didScroll = false;

    function Plugin(element, options) {
        this.element = element;
        this.$element = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }
    Plugin.prototype.init = function() {
        var self = this;
        this.startPosition = this.$element.position().top;
        this.offsetTop = this.$element.offset().top;
        this.height = this.$element.outerHeight(true);
        this.velocity = this.$element.attr('data-velocity');
        this.bgStart = parseInt(this.$element.attr('data-fit'), 10);
        $(document).scroll(function() {
            self.didScroll = true;
        });
        setInterval(function() {
            if (self.didScroll) {
                self.didScroll = false;
                self.scrolly();
            }
        }, 10);
    };
    Plugin.prototype.scrolly = function() {
        var dT = $(window).scrollTop(),
            wH = $(window).height(),
            pH = $('#parallax').height();
        position = this.startPosition;
        if (this.offsetTop >= (dT + wH)) {
            this.$element.addClass('scrolly-invisible');
        } else {
            if (this.$element.hasClass('scrolly-invisible')) {
                position = this.startPosition + (dT + (wH - this.offsetTop)) * this.velocity;
            } else {
                position = this.startPosition + dT * this.velocity;
            }
        }
        if (this.bgStart) {
            position = position + this.bgStart;
        }
        if (this.options.bgParallax === true) {
            this.$element.css({
                backgroundPosition: '50% ' + position + 'px'
            });
        } else {
            this.$element.css({
                top: position
            });
        }
    };
    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };
    $('.parallax').scrolly({
        bgParallax: true
    });
    $("#menu-icon").click(function() {
        $("#resp-menu").stop(true, true).delay(120).slideToggle(240);
        return false;
    });
});
$(window).scroll(function() {
    var top = $(this).scrollTop(),
        p = $("#floating-text-over_parallax").position(),
        pheight = $("#floating-text-over_parallax").height(),
        h = $("#fixed-header").height(),
        mA = p.top + (pheight / 2);
    if (!$("#parallax").is(":hidden")) {
        if (top <= 0) {
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, 0)"
            });
        } else if (top < mA) {
            var dif = (top + h) / mA;
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, " + dif + ")"
            });
        } else {
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, 1)"
            });
        }
    }
});
$(window).resize(function() {
    var top = $(this).scrollTop(),
        p = $("#floating-text-over_parallax").position(),
        pheight = $("#floating-text-over_parallax").height(),
        h = $("#fixed-header").height(),
        mA = p.top + (pheight / 2);
    if ($(window).width() > 500) {
        if (top <= 0) {
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, 0)"
            });
        } else if (top < mA) {
            var dif = (top + h) / mA;
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, " + dif + ")"
            });
        } else {
            $("#fixed-header").css({
                background: "rgba(18, 140, 171, 1)"
            });
        }
    } else {
        $("#fixed-header").css("background", "");
    }
});
$(window).scroll(function() {
    if ($(this).scrollTop() > 0) {
        $("#fixed-header").addClass("scrolled");
    } else {
        $("#fixed-header").removeClass("scrolled");
    }
});
$(function() {
    $("#parallax").css("background-size", "cover");
});

function isIE( version, comparison ){
    var $div = $('<div style="display:none;"/>').appendTo($('body'));
    $div.html('<!--[if '+(comparison||'')+' IE '+(version||'')+']><a>&nbsp;</a><![endif]-->');
    var ieTest = $div.find('a').length;
    $div.remove();
    return ieTest;
}




if(isIE(8)){ 
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#fixed-header').css('background', 'rgb(18,140,171)');
        } else {
            $('#fixed-header').css('background', '');
        }
    });
    
    
    $(window).resize(function() {
        var top = $(this).scrollTop();
        if (top <= 0) {
            $("#fixed-header").css('background','');
        } else {
            $("#fixed-header").css({
                background: "rgb(18,140,171)"
            });
        }
    });
    
    
    
}