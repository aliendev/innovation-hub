var gridElement = $('#grid-product-container');





// append each item in the json correctly to the product grid
$(innoList.list).each( function(index) 
{    
    
    var $this = $(this),
        $partnersInside = $("<span></span>"),
        $partnerCount = this.partnersInside.length,
        $productsInside = $("<div></div>");

    $(this.productsInside).each(function(i,v) 
    { 
        $productsInside.append(
            '<p><a href="' + v.url + '">' + v.name + '</a></p>'
        );
    });
    
    $(this.partnersInside).each(function(i,v)
    {
        $partnersInside.append(
            '<a href="' + v.url + '">' + v.name + '</a>' + comma($partnerCount, i)
        );
    });
    
    $(gridElement).append(
            '<div class="grid-product clearfix">' +
            '<div class="hoverstate">' +
                '<p class="product-name">' + this.name + '</p>' +
                '<p>' + this.tagline + '</p>' +
                $productsInside.html() + 
                '<p>Partner: ' + $partnersInside.html() + '</p>' +
                //'<p>Date Released: ' + this.releaseDate + '</p>' +
                '<p><a class="linkBtn" href="' + this.innoURL + 
                    '" title="find out more about ' + this.name + 
                    '">find out more</a></p>' +
            '</div>' +
            '<img src="' + this.image + '" class="image product-grid-image" />' +
            '<p class="product-title">' + this.name + '</p>' +
            '<p class="product-description">' + this.description + '</p>' +
        '</div>'
    );
});




function comma(e,n)
{
    if (n+1 < e)
    {
        return ", ";
    }
    return "";
}





$("#product-grid-container").simplePagination(
{
	pagination_container: '#grid-product-container',
	html_prefix: 'pagi',
	navigation_element: 'a',//button, span, div, et cetera
	items_per_page: 6,
	number_of_visible_page_numbers: 6,
	//
	use_page_numbers: true,
	use_first: true,
	use_previous: true,
	use_next: true,
	use_last: true,
	//
	use_page_x_of_x: false,
	use_page_count: false,// Can be used to combine page_x_of_x and specific_page_list
	use_showing_x_of_x: false,
	use_item_count: true,
	use_items_per_page: false,
	use_specific_page_list: false,
	//
	first_content: 'First',  //e.g. '<<'
	previous_content: '&nbsp; ',  //e.g. '<'
	next_content: '&nbsp; ',  //e.g. '>'
	last_content: 'Last', //e.g. '>>'
	page_x_of_x_content: 'Page',
	showing_x_of_x_content: 'Showing',
});


