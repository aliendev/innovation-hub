TODO: [Gitlab CE](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md),
update to suit my needs and use as a good example of what a contributing doc needs to be.

make good use of Gitlab markdown: https://docs.gitlab.com/ee/user/markdown.html

always keep in mind the quick actions available to us: https://gitlab.com/help/user/project/quick_actions
